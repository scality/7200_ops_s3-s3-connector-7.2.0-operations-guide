﻿<?xml version="1.0" encoding="utf-8"?>
<html MadCap:lastBlockDepth="6" MadCap:lastHeight="877" MadCap:lastWidth="648" xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd">
    <head>
        <link href="../Resources/TableStyles/SimpleDefinitions.css" rel="stylesheet" MadCap:stylesheetType="table" /><title>Site Recovery Procedures</title>
    </head>
    <body>
        <p class="ChapterNumber">13 </p>
        <h1 MadCap:autonum="1. &#160;">Site Recovery Procedures</h1>
        <p style="text-align: left;">In a multi-site deployment where one or more sites are lost, site recovery procedures are available to help regain the quorum of the RAFT sessions (or increase the number of available nodes to cover in a situation where more could be down).</p>
        <p><MadCap:variable name="S3 Connector Variables.ComponentName" /> supports multi-site deployments for metro-area networks, with low-latency network connections between the sites (connected within 10ms network round trip latencies). These are termed <i>stretched</i> deployments in that they leverage a single logical copy of the Scality RING, with synchronous updates on the machines deployed in the data centers that comprise the deployment. <MadCap:variable name="S3 Connector Variables.ComponentName" /> supports two-site (and also three-site) stretched deployments, with the benefit that if one of the data centers is lost or becomes inaccessible for a period of time, the administrator can use the recovery mechanisms described in this chapter to restore service availability from the remaining site(s). Full details on installing and operating two-site and three-site stretched <MadCap:variable name="S3 Connector Variables.ComponentName" /> deployments are provided in the <MadCap:variable name="General.SetupInstall" />.</p>
        <p>The main requirement for recovering the service is to have previously deployed warm standby servers (referred to as <i>WSBs</i> in the documentation). A warm standby is a metadata node substitute, also referred to as a <i>shadow</i> node that stays up-to-date by shadowing one active node of the current RAFT session (using the backups stored in the RING). </p>
        <p style="text-align: left;">For more information about the supported <MadCap:variable name="S3 Connector Variables.ComponentName" /> stretched deployment models, refer to the <MadCap:variable name="General.SetupInstall" />.</p>
        <h2 MadCap:autonum="1.1 &#160;">Recovering a Two-Site, Six-Server Deployment</h2>
        <p>In a two-site deployment, when one site is lost, there are a few manual operations to perform in order to resume service. Once the lost site comes up again, it will first have to catch up and synchronize itself with the active site. Once they are in the same state, the service will be briefly interrupted again, in order to return to nominal dual site state.</p>
        <table style="width: 100%;mc-table-style: url('../Resources/TableStyles/SimpleDefinitions.css');" class="TableStyle-SimpleDefinitions" cellspacing="0">
            <col class="TableStyle-SimpleDefinitions-Column-Column1" />
            <col class="TableStyle-SimpleDefinitions-Column-Column1" />
            <tbody>
                <tr class="TableStyle-SimpleDefinitions-Body-Row1">
                    <td class="TableStyle-SimpleDefinitions-BodyE-Column1-Row1">
                        <p class="InTableFormatted"><i>majority</i> site</p>
                    </td>
                    <td class="TableStyle-SimpleDefinitions-BodyD-Column1-Row1">
                        <p class="InTableFormatted">Three servers are active metadata nodes.</p>
                    </td>
                </tr>
                <tr class="TableStyle-SimpleDefinitions-Body-Row1">
                    <td class="TableStyle-SimpleDefinitions-BodyB-Column1-Row1">
                        <p class="InTableFormatted"><i>minority</i> site </p>
                    </td>
                    <td class="TableStyle-SimpleDefinitions-BodyA-Column1-Row1">
                        <p class="InTableFormatted">Two servers are active metadata nodes, and one is a WSB shadowing one of the servers on the majority site.</p>
                    </td>
                </tr>
            </tbody>
        </table>
        <p style="text-align: left;">The playbooks for the procedure uses Ansible options:</p>
        <table style="width: 100%;mc-table-style: url('../Resources/TableStyles/SimpleDefinitions.css');" class="TableStyle-SimpleDefinitions" cellspacing="0">
            <col class="TableStyle-SimpleDefinitions-Column-Column1" />
            <col class="TableStyle-SimpleDefinitions-Column-Column1" />
            <tbody>
                <tr class="TableStyle-SimpleDefinitions-Body-Row1">
                    <td class="TableStyle-SimpleDefinitions-BodyE-Column1-Row1" style="font-size: 9pt;">
                        <p class="InTableFormatted"><em>-l</em>
                        </p>
                    </td>
                    <td class="TableStyle-SimpleDefinitions-BodyD-Column1-Row1" style="font-size: 9pt;">
                        <p class="InTableFormatted"><span style="font-size: 9pt;">Option that allows playbooks to be run on only certain hosts. Otherwise, the playbook will fail with unreachable hosts.</span>
                        </p>
                    </td>
                </tr>
                <tr class="TableStyle-SimpleDefinitions-Body-Row1">
                    <td class="TableStyle-SimpleDefinitions-BodyB-Column1-Row1" style="font-size: 9pt;">
                        <p class="InTableFormatted"><i>-e</i>
                        </p>
                    </td>
                    <td class="TableStyle-SimpleDefinitions-BodyA-Column1-Row1" style="font-size: 9pt;">
                        <p class="InTableFormatted"><span style="font-size: 9pt;">Option that allows custom parameters to be provided to the playbooks (overriding their values in the inventory). Two main custom parameters are used, </span><i style="font-size: 9pt;">lost_site</i><span style="font-size: 9pt;"> (the group representing the lost site) and </span><i style="font-size: 9pt;">env_metadata_force_leader_on_site</i><span style="font-size: 9pt;"> (forces the leader of a RAFT session on a given site).</span>
                        </p>
                    </td>
                </tr>
            </tbody>
        </table>
        <h3 MadCap:autonum="1.1.1 &#160;">Minority Site Loss</h3>
        <p>In a two-site, six-server deployment, if the minority site is lost, the service becomes temporarily unavailable as the RAFT session leader is forced on that site. Allowing the leader to temporarily be on the majority site will resume service. Once the minority site is back up, it will need to catch up with the majority site. After monitoring this catch up, service will be temporarily disabled while returning to nominal state.</p>
        <p class="Note">A site loss is usually associated with power outage, fire, or other unpredictable event. If none of the machines on the minority site can be contacted (using <i>ping</i>, for example) the site has been lost. If the servers on the minority site can be contacted, but there is trouble with the S3 service, this is not the right procedure to follow.</p>
        <h4>Step 1: Allow the Leader to be on the Majority Site</h4>
        <p style="text-align: left;">Allowing the leader to be on the majority site will resume the service. This is done through the <i>run.yml</i> <i>playbook</i>, using the <i>DR</i> tag (thus running only certain steps associated with that tag). The changes are applied only to the majority site servers.</p>
        <p class="codeparatext_lastline" style="text-align: left;">ansible-playbook -i env/{{ ENVDIR }}/inventory run.yml -t DR -l majority -e <br />"env_metadata_force_leader_on_site=majority" -s</p>
        <p>Having just reconfigured the metadata nodes from the Majority site, these nodes must be restarted to ensure the update is taken into account.</p>
        <p class="codeparatext_lastline">
ansible -i ./env/{{ ENVDIR }}/inventory all -l majority -m shell -a 'docker restart scality-metadata-bucket-repd' -s</p>
        <h4>Step 2: Confirm Read/Write Access</h4>
        <p>Simply try listing a bucket, or putting an object to one. If this is not possible, you may want to check if that bucket is associated with a RAFT leader:</p>
        <p class="codeparatext_lastline" style="text-align: left;">$ curl http://{{ MAJORITYSERVERIP }}:9000/default/leader/{{ BUCKETNAME }}</p>
        <h4>Step 3: Return of the Lost Site</h4>
        <p class="Note">The procedure cannot be continued until the minority site returns.</p>
        <p>In case of a hardware loss, it is necessary to reinstall the software (refer to <MadCap:xref href="S3 Metadata Disk Replacement.htm#Rerun">"Rerun the Metadata Service" on page 1</MadCap:xref>). Thereafter, only the WSB state can be forced.</p>
        <h4>Step 4: Force Warm Standby Behavior on All Minority Servers</h4>
        <p>In order for the minority servers to catch up with the majority RAFT session, WSB behaviour must be temporarily forced on all of them:</p><pre xml:space="preserve">$ ansible-playbook -i env/{{ ENVDIR }}/inventory -l minority 
     tooling-playbooks/force_warm_stand_by.yml -s</pre>
        <p class="Note">In case of decoupled setup, the value given to the <span class="Code_Terminal">-l</span> option of ansible
			(it should be a coma-separated list of hosts or group names) must be
			augmented with the groups of still-reachable stateless connectors; as
			the configurations of the services directly accessing MetaData may
		require an update.</p>
        <h4>Step 5: Wait for the Minority Site to Catch Up with the RING </h4>
        <p>A playbook is provided to monitor catch-up. Until it is concluded that the minority and majority values (number of batch backups to the RING) are the same, this step should be repoated.</p>
        <p class="codeparatext_lastline" style="text-align: left;">$ ansible-playbook -i  ./env/{{ ENVDIR }}/inventory tooling-playbooks/<br />check-warmstandby-recover.yml -e 'recovering="minority" active="majority"' -s</p>
        <h4>Step 6: Resume Active Nodes on the Minority Site</h4>
        <p>Once the values are the same on both sites, WSB behaviour no longer needs to be forced on all minority machines. Once again having two active nodes of the RAFT session on the minority site will allow for more granular catch up with the RAFT session leader.</p>
        <p class="codeparatext_lastline" style="text-align: left;">$ ansible-playbook -i env/{{ ENVDIR }}/inventory <br />tooling-playbooks/return_to_normal.yml -s</p>
        <h4>Step 7: Wait for Minority Active Nodes to Catch Up with the Leader</h4>
        <p>A playbook is provided to monitor catch up. Until it concludes that the minority and majority values (RAFT counter for metadata operations) are the same, this step should be repeated.</p>
        <p class="codeparatext_lastline" style="text-align: left;">$ ansible-playbook -i ./env/{{ ENVDIR }}/inventory <br />tooling-playbooks/check-warmstandby-leader-switch.yml</p>
        <p class="Alert">Once the values are the same on both sites, the service must be stopped while returning to nominal state (enacted simply by stopping all the S3 containers). Once this is done, the previous step must be repeated to ensure that all operations are known to both sites.</p>
        <p class="codeparatext_lastline" style="text-align: left;">$ ansible -i ./env/{{ ENVDIR }}/inventory all -m shell -a <br />'docker stop scality-s3' -s</p>
        <p class="codeparatext_lastline" style="text-align: left;">$ ansible-playbook -i ./env/{{ ENVDIR }}/inventory tooling-playbooks/<br />check-warmstandby-leader-switch.yml</p>
        <h4>Step 8: Return to Nominal State</h4>
        <p style="page-break-after: avoid;">The leader must eventually be forced on the minority site again, to cover for a potential upcoming majority site loss. To do this, (1) run the <span class="FileName">run.yml</span> playbook with the <span class="ProprietaryNonConventional">DR</span> tag, (2) restart the reconfigured metadata nodes, and (3) restart the s3 container to resume service.</p>
        <div style="page-break-inside: avoid;">
            <p class="codeparatext_lastline">$ ansible-playbook -i ./env/{{ ENVDIR }}/inventory run.yml -t DR -s </p>
            <p class="codeparatext">$ ansible -i ./env/{{ ENVDIR }}/inventory all -m shell \</p>
            <p class="codeparatext_indent_lastline">-a 'docker restart scality-metadata-bucket-repd' -s
</p>
            <p class="codeparatext"> $ ansible -i ./env/{{ ENVDIR }}/inventory all -m shell \ </p>
            <p class="codeparatext_indent_lastline">-a 'docker restart scality-metadata-bucket-repd' -s   </p>
            <p class="codeparatext_lastline">$ ansible -i ./env/{{ ENVDIR }}/inventory all -m shell -a 'docker start scality-s3' -s
</p>
        </div>
        <p class="Alert"><b> The service must be resumed upon completion.</b>
        </p>
        <h3 MadCap:autonum="1.1.2 &#160;">Majority Site Loss</h3>
        <p>In a two-site, six-server deployment, if the majority site is lost, the service becomes temporarily unavailable as the RAFT session has only two active nodes on the minority site. Temporarily activating the WSB on the minority site will resume service. Once the majority site is back up, all of its servers should be forced into WSB mode until they catch up with the minority site. Monitor this catch up. When resuming to nominal state, there will be a short service loss as the minority site WSB resumes that function, and the majority site nodes become active nodes again.</p>
        <p class="Note">A site loss is usually associated with power outage, fire, or other unpredictible event. If none of the machines on the majority site answer to ping, the site is lost. If the minority servers answer to ping, but the S3 service is not behaving as expected this is not the right procedure to follow.</p>
        <h4>Step 1: Make the Warm Standby Server Active</h4>
        <p>Making the minority warm standby an active node will resume the service. A specific playbook exists for this purpose: it must be told which site is lost, and on which site the warm standby should be activated.</p>
        <p class="codeparatext_lastline" style="text-align: left;">ansible-playbook -i env/{{ ENVDIR }}/inventory -l minority \<br />-e 'lost_sites="majority"' tooling-playbooks/activate_warm_stand_by.yml -s</p>
        <h4>Step 2: Confirm Read/Write Access</h4>
        <p>Simply try listing a bucket or putting an object to one. If it fails, check if that bucket is associated with a RAFT leader:</p>
        <p class="codeparatext_lastline">curl http://{{ MINORITYSERVERIP }}:9000/default/leader/{{ BUCKETNAME }}</p>
        <h4>Step 3: Return of Lost Site</h4>
        <p>The procedure cannot be continued until the minority site returns.</p>
        <h4>Step 4: Force Warm Standby Behavior on All Majority Servers</h4>
        <p>In order for the minority servers to catch up with the minority RAFT session, all of them will be forced to temporarily have a WSB behavior:</p>
        <p class="codeparatext_lastline" style="text-align: left;">ansible-playbook -i env/{{ ENVDIR }}/inventory -l majority \<br />tooling-playbooks/force_warm_stand_by.yml -s</p>
        <p class="Note">In case of decoupled setup, the value given to the <span class="Code_Terminal">-l</span> option of ansible
			(it should be a coma-separated list of hosts or group names) must be
			augmented with the groups of still-reachable stateless connectors; as
			the configurations of the services directly accessing MetaData may
		require an update.</p>
        <h4>Step 5: Wait for the Minority Site to Catch Up with the RING</h4>
        <p>A playbook is provided to monitor the catch up. Until it concludes that the minority and majority values (number of batch backups to the RING) are the same, repeat this step.</p>
        <p class="codeparatext_lastline" style="text-align: left;">ansible-playbook -i  ./env/{{ ENVDIR }}/inventory \<br />tooling-playbooks/check-warmstandby-recover.yml \<br />-e 'recovering="majority" active="minority"' -s</p>
        <h4>Step 6: Return to Nominal State</h4>
        <p>Once the are the same on both sites, the majority machines can resume as active nodes. Returning to having three active nodes on the majority site, and one warm standby on the minority site will incur a short downtime while the metadata containers restart with their new config.</p>
        <p class="codeparatext_lastline" style="text-align: left;">ansible-playbook -i env/{{ ENVDIR }}/inventory \<br />tooling-playbooks/return_to_normal.yml -s</p>
    </body>
</html>